from flask import Flask, render_template, jsonify
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello Geddy, How are You?"

@app.route("/home")
def home():
    return """Welcome to the home, 
    please replace the home in the address bar with...
    /user, /user/anythingYouWant, /data, /data/more, /secrets. """

@app.route("/user")
def user():
	return render_template("profile.html")

@app.route("/user/<username>")
def username(username):
	return render_template("profile.html", name=username)

@app.route("/data")
def get_data():
	data = {"igloo": "club pengiun",
			"ip": 1234,
			"cyclpos_name": 3.4,
			"Stopped_train_plane": False
			}
	return jsonify(data)
@app.route("/data/more")
def get_more_data():
	data = {"arr": "mr.krabs",
			"pi": 3.14,
			"more_data": "stop it.",
			"arr again": "sponge bob pirates of vengance",
			"crippling chippido": "pringal moustache",
			"nil": False
			}
	return jsonify(data)
@app.route("/secrets")
def private_things():
	return render_template("test.html")

if __name__ == "__main__":
    app.run(debug = True)


